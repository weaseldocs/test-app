package handlers

import (
	"context"
	"database/sql"
	"encoding/json"
	"net/http"

	"github.com/gin-gonic/gin"
)

type User struct {
	CNOMALU string
	CNRODNI string
	CCLAVE  string
	CESTADO string
	ACODIGO []Codigo
}

type Codigo struct {
	CCODALU string
	CUNIACA string
	CNOMUNI string
}

func Login(c *gin.Context) {
	user := &User{}
	_ = json.NewDecoder(c.Request.Body).Decode(user)

	// Validar parámetros
	if len(user.CNRODNI) != 8 {
		c.String(http.StatusBadRequest, "NÚMERO DE DNI NO VÁLIDO")
		return
	}
	// if user.CCLAVE == "" {
	// 	sendError(rw, http.StatusBadRequest, "CONTRASEÑA NO DEFINIDA")
	// 	return
	// }

	login(c, user)
}

func login(c *gin.Context, user *User) {
	query := `SELECT cNombre, cClave, cEstado FROM S01MPER WHERE
							(cTipDoc = '1' AND cNroDni = $1) OR 
							(cTipDoc = '2' AND cDocExt = $1) 
				ORDER BY cTipDoc LIMIT 1`
	// clave := sha512.New()
	// clave.Write([]byte(user.CCLAVE))
	// clahex := hex.EncodeToString(clave.Sum(nil))
	err := XPPool.QueryRow(context.Background(), query, user.CNRODNI).Scan(&user.CNOMALU, &user.CCLAVE, &user.CESTADO)
	if err != nil {
		if err == sql.ErrNoRows {
			c.String(http.StatusBadRequest, "NO SE ENCONTRÓ DNI")
			return
		}
		c.String(http.StatusInternalServerError, "ERROR AL RECUPERAR DNI")
		return
	}
	user.CCLAVE = ""
	// if clahex != user.CCLAVE {
	// 	sendError(rw, http.StatusBadRequest, "CONTRASEÑA INCORRECTA")
	// 	return
	// }
	// if user.CESTADO == "X" {
	// 	sendError(rw, http.StatusBadRequest, "USUARIO NO SE ENCUENTRA ACTIVO")
	// 	return
	// }
	// query = `SELECT A.cCodAlu, B.cNomUni, B.cUniAca FROM A01MALU A
	// 			INNER JOIN S01TUAC B ON B.cUniAca = A.cUniAca
	// 			WHERE A.cNroDni = $1 AND A.cEstado = 'A' and B.cNivel in ('01', '02', '03')
	// 			ORDER BY A.cCodAlu DESC`
	// rows, err := dbpool.Query(context.Background(), query, user.CNRODNI)
	// if err != nil {
	// 	sendError(rw, http.StatusInternalServerError, "ERROR DE EJECUCIÓN DE BASE DE DATOS")
	// }
	// codigos := []Codigo{}

	// for rows.Next() {
	// 	codigo := Codigo{}
	// 	if err := rows.Scan(&codigo.CCODALU, &codigo.CNOMUNI, &codigo.CUNIACA); err != nil {
	// 		sendError(rw, http.StatusInternalServerError, "ERROR LEYENDO RESPUESTA DE LA BASE DE DATOS")
	// 		return
	// 	}
	// 	codigos = append(codigos, codigo)
	// }

	// user.ACODIGO = codigos
	// user.CCLAVE = ""

	c.JSON(http.StatusOK, user)
}
