package handlers

import (
	"context"
	"net/http"
	"strings"

	"github.com/gin-gonic/gin"
)

type Compra struct {
	CCODART string
	CDESCRI string
}

func RecuperarCompras(c *gin.Context) {
	// _ = json.NewDecoder(r.Body).Decode(user)

	recuperarCompras(c)
}

func recuperarCompras(c *gin.Context) {
	query := `SELECT d.idkard, d.numsec, d.cantid, d.codart, a.descri, a.unidad, u.descri,
					d.indmov, d.indigv, d.cossol, d.valsol, d.cosdol, d.valdol
				FROM KardexDet AS d
				LEFT JOIN Kardex AS k ON k.idkard = d.idkard
				LEFT JOIN Articulos AS a ON a.codart = d.codart AND a.uniope = k.uniope
				LEFT JOIN UnidadMedida AS u ON u.unidad = a.unidad
				WHERE k.tipkar in ('C', 'D')
				ORDER BY d.idkard, d.numsec`

	rows, err := AlvPool.Query(context.Background(), query)
	if err != nil {
		c.String(http.StatusInternalServerError, "ERROR DE EJECUCION DE BASE DE DATOS")
		return
	}
	compras := []Compra{}

	for rows.Next() {
		values, err := rows.Values()

		if err != nil {
			c.String(http.StatusInternalServerError, "ERROR LEYENDO RESPUESTA DE LA BASE DE DATOS")
			return
		}
		compra := Compra{}
		compra.CCODART = strings.TrimSpace(values[3].(string))
		compra.CDESCRI = values[4].(string)
		compras = append(compras, compra)
	}

	c.JSON(http.StatusOK, compras)
}
