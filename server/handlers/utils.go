package handlers

import (
	"context"
	"os"

	"github.com/jackc/pgx/v4/pgxpool"
)

var AlvPool *pgxpool.Pool
var XPPool *pgxpool.Pool

func InitAlv() (*pgxpool.Pool, error) {
	return pgxpool.Connect(context.Background(), os.Getenv("DB_ALV"))
}

func InitXP() (*pgxpool.Pool, error) {
	return pgxpool.Connect(context.Background(), os.Getenv("DB_XP"))
}

// func sendError(rw http.ResponseWriter, code int, message string) {
// 	type response struct {
// 		Message string
// 	}
// 	res := &response{Message: message}
// 	rw.WriteHeader(code)
// 	json.NewEncoder(rw).Encode(res)
// }

// func sendJSON(rw http.ResponseWriter, code int, payload interface{}) {
// 	response, _ := json.Marshal(payload)
// 	rw.Header().Set("Content-Type", "application/json")
// 	rw.WriteHeader(code)
// 	rw.Write(response)
// }
