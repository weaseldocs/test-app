<?php
// Conexion a Base de Datos
class CSql {
   public $pcError, $pnNumRow;
   protected $h;

   public function __construct() {
      $this->pcError = null;
      $this->pnNumRow = 0;
   }

   public function omDisconnect() {
      $this->omExec("COMMIT;");
      pg_close($this->h);
   }

   public function omConnect($lnFlag = 0) {
      if ($lnFlag == 1) {
         $lcConStr = "host=localhost dbname=UCSMListener port=5432 user=postgres password=postgres";
      } else {
         $lcConStr = "host=10.0.7.170 dbname=UCSMFactElec port=5432 user=u70840304 password=70840304";
         //$lcConStr = "host=10.0.7.170 dbname=UCSMERP port=5432 user=ucsmerpweb password=12UC\$M3RP34";
      }
      try {
         @$this->h = pg_connect($lcConStr);
      } catch (Exception $ex) {
         $this->pcError = "No se pudo conectar a la base de datos";
         return false;
      }
      $this->omExec("BEGIN;");
      return true;
   }

   public function omExec($p_cSql) {
      $lcSql = substr(strtoupper(trim($p_cSql)), 0, 6);
      if ($lcSql === "SELECT") {
         $this->pnNumRow = 0;
         $RS = pg_query($this->h, $p_cSql);
         if (!($RS)) {
            $this->pcError = "Error al ejecutar comando SQL";
            return false;
         }
         $this->pnNumRow = pg_num_rows($RS);
         return $RS;
      } else {
         @$RS = pg_query($this->h, $p_cSql);
         if (pg_affected_rows($RS) == 0)
            if (!($RS)) {
               $this->pcError = "La operacion no afecto a ninguna fila";
               return false;
            }
         return true;
      }
   }

   public function fetch($RS) {
      return pg_fetch_row($RS);     
   }
   
   public function rollback() {
      $this->omExec("ROLLBACK;");
   }
}

?>
