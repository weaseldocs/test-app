<?php
/*
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
*/
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token , Authorization, X-Requested-With");
header("Access-Control-Allow-Methods: POST");
header("Content-type: application/json");
header("Allow: POST");

require_once "CSql.php";
$p_oSql = new CSql();
$p_oSql->omConnect();
$lcSql = "SELECT d.idkard, d.numsec, d.cantid, d.codart, a.descri, a.unidad, u.descri,
            d.indmov, d.indigv, d.cossol, d.valsol, d.cosdol, d.valdol
            FROM KardexDet AS d
            LEFT JOIN Kardex AS k ON k.idkard = d.idkard
            LEFT JOIN Articulos AS a ON a.codart = d.codart AND a.uniope = k.uniope
            LEFT JOIN UnidadMedida AS u ON u.unidad = a.unidad
            WHERE k.tipkar in ('C', 'D')
            ORDER BY d.idkard, d.numsec";
$R1 = $p_oSql->omExec($lcSql);
$laDatos = [];
while ($laFila = $p_oSql->fetch($R1)) {
   $laTmp = [
      "CCODART" => $laFila[3],
      "CDESCRI" => $laFila[4],
   ];
   array_push($laDatos, $laTmp);
}

echo json_encode($laDatos);

?>
