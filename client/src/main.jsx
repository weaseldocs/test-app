import React from "react";
import ReactDOM from "react-dom";
import App from "./App";

import { ChakraProvider, CSSReset, extendTheme, Box } from "@chakra-ui/react";

const colors = {
   primary: "#05be6a",
   secondary: "#fbc804",
};

const theme = extendTheme({ colors });

ReactDOM.render(
   <React.StrictMode>
      <ChakraProvider theme={theme}>
         <CSSReset />
         <App />
      </ChakraProvider>
   </React.StrictMode>,
   document.getElementById("root")
);
