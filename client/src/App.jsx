import { useState, useRef, Fragment } from "react";
import { set, useForm } from "react-hook-form";
import { FixedSizeList, FixedSizeGrid } from "react-window";
import axios from "axios";

import { api } from "./api";
import {
   FormErrorMessage,
   FormLabel,
   FormControl,
   Input,
   Button,
   AlertDialog,
   AlertDialogBody,
   AlertDialogFooter,
   AlertDialogHeader,
   AlertDialogContent,
   AlertDialogOverlay,
   Text,
   useDisclosure,
   Table,
   Thead,
   Tbody,
   Tr,
   Th,
   Td,
   TableContainer,
   VStack,
   StackDivider,
   Box,
   HStack,
   Flex,
} from "@chakra-ui/react";

function App() {
   const [user, setUser] = useState([]);
   const [compras, setCompras] = useState([]);
   const [total, setTotal] = useState(0);
   // const { isOpen, onOpen, onClose } = useDisclosure();
   // const closeRef = useRef();

   const {
      handleSubmit,
      register,
      formState: { errors, isSubmitting },
   } = useForm();

   const Row = ({ index, style }) => (
      // <Table style={style} variant="simple">
      // </Table>
      <Tr style={style}>
         <Td>{compras[index].CCODART}</Td>
         <Td>{compras[index].CDESCRI}</Td>
      </Tr>
   );

   async function onSubmit(values) {
      setTotal(0);
      try {
         var start = new Date();
         let response = await api.post("/login", values);
         console.log(response);
         setUser(response.data);
         var end = new Date();
         setTotal(end - start);
      } catch (error) {
         alert(error.response.data.Message);
      }
   }

   async function onSubmitGo() {
      setTotal(0);
      try {
         var start = new Date();
         let response = await api.post("/recuperarCompras");
         setCompras(response.data);
         var end = new Date();
         setTotal(end - start);
      } catch (error) {
         alert(error.response.data.Message);
      }
   }

   async function onSubmitPhp() {
      setTotal(0);
      try {
         var start = new Date();
         let response = await axios.post("https://ibas.ucsm.edu.pe/UCSMERP/Wsr9998.php");
         // let response = await axios.post("http://10.0.7.163/UCSMERP/Wsr9998.php");
         // let response = await axios.post("http://localhost/Wsr9998.php");
         setCompras(response.data);
         var end = new Date();
         setTotal(end - start);
      } catch (error) {
         alert(error.response.data.Message);
      }
   }

   async function onSubmitFlask() {
      setTotal(0);
      try {
         var start = new Date();
         // let response = await axios.post("http://10.0.7.163/UCSMERP/Wsr9998.php");
         let response = await axios.post("https://ibas.ucsm.edu.pe/flask/test");
         // let response = await axios.post("http://localhost/flask/tests");
         setCompras(response.data);
         var end = new Date();
         setTotal(end - start);
      } catch (error) {
         alert(error.response.data.Message);
      }
   }
   return (
      <Flex direction="column" minHeight="100vh">
         <Box h="4.5rem" bg="primary" textAlign="center" padding="1rem">
            <Text fontSize="2xl" fontWeight="bold" color="white">
               Weasel Test
            </Text>
         </Box>
         <Flex flex="1" direction="column" justifyContent="center">
            <Box p={4}>
               <form onSubmit={handleSubmit(onSubmitPhp)}>
                  <Button mt={4} colorScheme="green" isLoading={isSubmitting} type="submit">
                     Submit PHP
                  </Button>
               </form>
               <form onSubmit={handleSubmit(onSubmitGo)}>
                  <Button mt={4} colorScheme="green" isLoading={isSubmitting} type="submit">
                     Submit Go
                  </Button>
               </form>
               <form onSubmit={handleSubmit(onSubmitFlask)}>
                  <Button mt={4} colorScheme="green" isLoading={isSubmitting} type="submit">
                     Submit Flask
                  </Button>
               </form>
               <form onSubmit={handleSubmit(onSubmit)}>
                  <FormControl isInvalid={!!errors.CNRODNI}>
                     <FormLabel htmlFor="nrodni">Usuario</FormLabel>
                     <Input
                        id="nrodni"
                        placeholder="Número de DNI"
                        {...register("CNRODNI", {
                           // required: "Este campo es necesario",
                           minLength: { value: 8, message: "La longitud mínima debe ser 8 caracteres" },
                        })}
                     />
                     <Text>{user?.CNRODNI}</Text>
                     <FormErrorMessage>{errors.CNRODNI && errors.CNRODNI.message}</FormErrorMessage>
                  </FormControl>
                  {/* <FormControl isInvalid={!!errors.CCLAVE}>
                     <FormLabel htmlFor="passwd">Contraseña</FormLabel>
                     <Input
                        id="passwd"
                        type="password"
                        placeholder="Contraseña"
                        {...register("CCLAVE", {
                           required: "Este campo es necesario",
                        })}
                     />
                     <FormErrorMessage>{errors.CCLAVE && errors.CCLAVE.message}</FormErrorMessage>
                  </FormControl> */}
                  <Button mt={4} colorScheme="green" isLoading={isSubmitting} type="submit">
                     Submit 3
                  </Button>
               </form>
               <Text>Tiempo de solicitud:</Text>
               <Text>{total} ms</Text>
               <TableContainer>
                  <Table variant="simple">
                     <Thead>
                        <Tr>
                           <Th>CODART</Th>
                           <Th>CDESCRI</Th>
                        </Tr>
                     </Thead>
                     {/* {compras?.map((item, idx) => (
                        <Tr key={`row-${idx}`}>
                        <Td>{item.CCODART}</Td>
                        <Td>{item.CDESCRI}</Td>
                        </Tr>
                     ))} */}
                  </Table>
                  <FixedSizeList
                     height={600}
                     itemCount={compras.length}
                     itemSize={50}
                     width="100%"
                     innerElementType={Table}
                  >
                     {Row}
                  </FixedSizeList>
               </TableContainer>

               {/* <AlertDialog isOpen={isOpen} leastDestructiveRef={closeRef} onClose={onClose} isCentered>
                  <AlertDialogOverlay>
                     <AlertDialogContent>
                        <AlertDialogHeader fontSize="2xl" fontWeight="bold">
                           Datos del usuario:
                        </AlertDialogHeader>

                        <AlertDialogBody>
                           <VStack divider={<StackDivider borderColor="gray.200" />} spacing={4} align="stretch">
                              <Box>
                                 <Text fontSize="lg" fontWeight="bold">
                                    Nombre:
                                 </Text>
                                 <Text>{user?.CNOMALU}</Text>
                              </Box>
                              <Box>
                                 <Text fontSize="lg" fontWeight="bold">
                                    Código(s) de alumno:
                                 </Text>
                                 <TableContainer>
                                    <Table variant="striped">
                                       <Thead>
                                          <Tr>
                                             <Th>Unidad Académica</Th>
                                             <Th>Cod. de Alumno</Th>
                                          </Tr>
                                       </Thead>
                                       <Tbody>
                                          {user?.ACODIGO?.map((item, idx) => (
                                             <Tr key={`row-${idx}`}>
                                                <Td>{item.CNOMUNI}</Td>
                                                <Td>{item.CCODALU}</Td>
                                             </Tr>
                                          ))}
                                       </Tbody>
                                    </Table>
                                 </TableContainer>
                              </Box>
                           </VStack>
                        </AlertDialogBody>

                        <AlertDialogFooter>
                           <Button colorScheme="green" onClick={onClose} ml={3}>
                              OK
                           </Button>
                        </AlertDialogFooter>
                     </AlertDialogContent>
                  </AlertDialogOverlay>
               </AlertDialog> */}
            </Box>
         </Flex>
      </Flex>
   );
}

export default App;
