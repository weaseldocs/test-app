import axios from "axios";

const api = axios.create({
   // baseURL: "http://localhost:9090",
   baseURL: "https://ibas.ucsm.edu.pe/api",
   // baseURL: "http://10.0.7.163/api",
});

export { api };
