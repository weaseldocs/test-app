import http from "k6/http";
import { sleep, check } from "k6";

export const options = {
  vus: 200,
  duration: "10s",
  // stages: [
  //   { duration: "30s", target: 200 },
  //   { duration: "10s", target: 100 },
  //   { duration: "20s", target: 0 },
  // ],
};

export default function () {
  // let phpUrl = "http://localhost:8080/index.php";
  // let goUrlLogin = "http://localhost:9090/iniciar-sesion";
  // let goUrlLogin = "http://10.11.103.20/api/iniciar-sesion";
  // let goUrlMenu = "http://localhost:9090/init-menu";
  let url = "http://10.0.7.170/api/recuperarCompras";

  const params = {
    headers: {
      "Content-Type": "application/json",
    },
  };

  const res = http.post(url, params);
  check(res, { "Status was 200": (r) => r.status == 200 });
}
