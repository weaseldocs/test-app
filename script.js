import http from "k6/http";
import { sleep, check } from "k6";

export const options = {
  vus: 500,
  duration: "30s",
  // stages: [
  //   { duration: "30s", target: 200 },
  //   { duration: "1m30s", target: 100 },
  //   { duration: "20s", target: 0 },
  // ],
};

// export const options = {
//   vus: 200,
//   duration: "1m30s",
//   // stages: [
//   //   { duration: "30s", target: 200 },
//   //   { duration: "1m30s", target: 100 },
//   //   { duration: "20s", target: 0 },
//   // ],
// };

export default function () {
  let phpUrl = "http://192.168.193.113";
  // let goUrl = "http://localhost:9090/";
  http.get(phpUrl);
  // const res = http.get(goUrl);
  // check(res, { "status was 200": (r) => r.status == 200 });
  sleep(1);
}
