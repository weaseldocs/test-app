import http from "k6/http";
import { sleep, check } from "k6";

export const options = {
  vus: 200,
  duration: "10s",
  // stages: [
  //   { duration: "30s", target: 200 },
  //   { duration: "10s", target: 100 },
  //   { duration: "20s", target: 0 },
  // ],
};

export default function () {
  let url = "http://10.0.7.170/UCSMERP/Wsr9998.php";

  const params = {
    headers: {
      "Content-Type": "application/json",
    },
  };

  const res = http.post(url, params);
  check(res, { "Status was 200": (r) => r.status == 200 });
}
